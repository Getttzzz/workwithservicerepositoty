package com.example.getttzzz.workwithservice;

import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.net.wifi.WifiManager;
import android.os.Handler;
import android.os.IBinder;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private Button btnStop;
    private Button btnStart;
    private Button btnBindToServ;
    private Button btnUnbindToServ;
    private Button btnResetStopwatch;
    private Button btnRefresh;
    private Button btnStartStopwatch;
    private Button btnPauseStopwatch;

    private TextView tvTime;
    boolean mBound = false;
    private TimeConnectionService mTimeConnectionService;
    final String LOG_TAG = "myLogs";

    private Handler mHandler = new Handler();
    private long startTime;
    private long elapsedTime;
    private final int REFRESH_RATE = 1000;
    private boolean stopped = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();

        startService(new Intent(getApplicationContext(), TimeConnectionService.class));

    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        refreshingTextview();
    }

    @Override
    protected void onStart() {
        super.onStart();
        bindToServiceFromMainActivity();
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mBound) {
            unbindService(mConnection);
            mBound = false;
        }
        mHandler.removeCallbacks(startTimer);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    private View.OnClickListener btnStartServiceListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            startService(new Intent(getApplicationContext(), TimeConnectionService.class));
        }
    };

    private View.OnClickListener btnStopServiceListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            stopService(new Intent(getApplicationContext(), TimeConnectionService.class));
        }
    };

    View.OnClickListener btnBindToServiceListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            bindToServiceFromMainActivity();
        }
    };

    View.OnClickListener btnUnbindToServiceListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            unbindToServiceFromMainActivity();
        }
    };

    View.OnClickListener btnResetStopwatchListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            //TODO add solution reset stopwatch

        }
    };

    View.OnClickListener btnRefreshListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            refreshingTextview();
        }
    };

    View.OnClickListener btnStartStopwatchListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Log.d(LOG_TAG, "MainActivity startStopwatch()");
            mTimeConnectionService.startStopwatch();
        }
    };

    View.OnClickListener btnPauseStopwatchListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Log.d(LOG_TAG, "MainActivity pauseStopwatch()");
            mTimeConnectionService.pauseStopwatch();
        }
    };


    public void bindToServiceFromMainActivity() {
        Intent intent = new Intent(getApplicationContext(), TimeConnectionService.class);
        bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
    }

    public void unbindToServiceFromMainActivity() {
        if (!mBound) return;
        unbindService(mConnection);
        mBound = false;
    }

    private Runnable startTimer = new Runnable() {
        public void run() {
            Log.d(LOG_TAG, "run() mTimeString = " + mTimeConnectionService.getTimeString());
            tvTime.setText(mTimeConnectionService.getTimeString());
            mHandler.postDelayed(this, REFRESH_RATE);
        }
    };

    private ServiceConnection mConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            Log.d(LOG_TAG, "MainActivity onServiceConnected");
            TimeConnectionService.LocalBinder binder = (TimeConnectionService.LocalBinder) service;
            mTimeConnectionService = binder.getService();
            mBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            Log.d(LOG_TAG, "MainActivity onServiceDisconnected");
            mBound = false;
        }
    };

    private void refreshingTextview() {
        if (stopped) {
            startTime = System.currentTimeMillis() - elapsedTime;
        } else {
            startTime = System.currentTimeMillis();
        }
        mHandler.removeCallbacks(startTimer);
        mHandler.postDelayed(startTimer, 0);
    }

    private void initView() {
        btnStart = (Button) findViewById(R.id.btn_start_service);
        btnStart.setOnClickListener(btnStartServiceListener);

        btnStop = (Button) findViewById(R.id.btn_stop_service);
        btnStop.setOnClickListener(btnStopServiceListener);

        btnBindToServ = (Button) findViewById(R.id.btn_bind_to_service);
        btnBindToServ.setOnClickListener(btnBindToServiceListener);

        btnUnbindToServ = (Button) findViewById(R.id.btn_unbind_to_service);
        btnUnbindToServ.setOnClickListener(btnUnbindToServiceListener);

        btnResetStopwatch = (Button) findViewById(R.id.btn_reset_stopwatch);
        btnResetStopwatch.setOnClickListener(btnResetStopwatchListener);

        btnRefresh = (Button) findViewById(R.id.btn_refresh_stopwatch);
        btnRefresh.setOnClickListener(btnRefreshListener);

        btnStartStopwatch = (Button) findViewById(R.id.btn_start_stopwatch);
        btnStartStopwatch.setOnClickListener(btnStartStopwatchListener);

        btnPauseStopwatch = (Button) findViewById(R.id.btn_pause_stopwatch);
        btnPauseStopwatch.setOnClickListener(btnPauseStopwatchListener);

        tvTime = (TextView) findViewById(R.id.tvTime);
    }
}
